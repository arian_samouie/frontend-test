import React from 'react';
import './sass/styles.scss';

// Components
import Pokedex from './components/Pokedex';

function App() {
  return (
    <div className="App">
        <Pokedex />
    </div>
  );
}

export default App;
