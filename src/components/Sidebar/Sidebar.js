import React from 'react';
import PokemonList from './PokemonList';

const Sidebar = (props) => {

    const {results , pokemonActive} = props;

    return (
        <div className="col-12 col-md-4 pokedex-sidebar">
            <h2 className="heading-two">Pok&eacute;dex</h2>
            <p className="body-copy">List of pok&eacute;mon</p>
            <PokemonList results={results} pokemonActive={pokemonActive}/>
        </div>
    );
};

export default Sidebar;