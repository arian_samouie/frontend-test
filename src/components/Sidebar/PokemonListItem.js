import React from 'react';

const PokemonListItem = (props) => {

    const {results, pokemonActive} = props;


    return (
        <li className="pokemon-list-item">
            <button className="pokemon-list-item-button theme-button" onClick={() => {pokemonActive(results)}}>
                {results.name}
            </button>
        </li>
    );
};

export default PokemonListItem;