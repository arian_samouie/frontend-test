import React from 'react';

const PokemonListPagination = () => {

    return (
        <ul className="pokemon-list-pagination">
            <li className="pagination-list-item">
                <button className="pagination-button">Prev</button>
            </li>
            <li className="pagination-list-item">
                <button className="pagination-button">Next</button>
            </li>
        </ul>
    );
};

export default PokemonListPagination;