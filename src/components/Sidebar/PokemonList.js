import React from 'react';
import PokemonListPagination from "./PokemonListPagination";
import PokemonListItem from "./PokemonListItem";

const PokemonList = (props) => {

    const {results, pokemonActive} = props;

    return (
        <React.Fragment>
            <ul className="pokemon-list">
                {results.map((data, key) => {
                    return <PokemonListItem results={data} pokemonActive={pokemonActive} key={key}/>
                })}
            </ul>
            <PokemonListPagination />
        </React.Fragment>
    );
};

export default PokemonList;