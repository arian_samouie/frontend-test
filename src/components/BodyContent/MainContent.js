import React from 'react';
import SearchBox from './SearchBox';
import PokemonProfile from './PokemonProfile';

const MainContent = (props) => {

    const {profileResult, seacrhPokemon} = props;

    return (
        <div className="col-12 col-md-8 main-content">
            <SearchBox seacrhPokemon={seacrhPokemon}/>
            {profileResult && <PokemonProfile profileResult={profileResult}/>}
        </div>
    );
};

export default MainContent;