import React from 'react';

const PokemonProfile = (props) => {

    const {profileResult} = props;
    const items = profileResult.abilities;
    const profileImage = profileResult.sprites && profileResult.sprites.back_default;

    return (
        <div className="pokemon-profile">
            <div className="profile-image">
                <img src={profileImage} alt="Pokemon Profile" className="profile-inner-image"/>
            </div>
            <div className="profile-info">
                <div className="col-12 col-md-5">
                    <h3 className="profile-name">{profileResult.name}</h3>
                    <h4 className="profile-body">Weight: {profileResult.weight} Kg</h4>
                    <h4 className="profile-body">Height: {profileResult.height} cm</h4>
                </div>
                <div className="col-12 col-md-7">
                    <h4 className="profile-body heading">Abilities</h4>
                    {items && items.map((data, key) => {
                        return (
                            <span className="ability-each" key={key}>{data.ability.name}</span>
                        )
                    })}
                </div>
            </div>

        </div>
    );
};

export default PokemonProfile;