import React from 'react';

const SearchBox = (props) => {

    const {seacrhPokemon} = props

    return (
        <div className="pokemon-search form-group">
            <input type="search" placeholder="Search" className="theme-input" onChange={seacrhPokemon}/>
        </div>
    );
};

export default SearchBox;