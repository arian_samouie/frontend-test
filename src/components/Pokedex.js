import React, {Component} from 'react';

// Components
import Sidebar from './Sidebar/Sidebar';
import MainContent from './BodyContent/MainContent';

class Pokedex extends Component {

    state = {
        results: [],
        selectedResult: [],
        searchTyped: [''],
        resultsLoad: {
            url: 'https://pokeapi.co/api/v2/pokemon/1/'
        }
    };

    seacrhPokemon(event) {
        this.setState({searchTyped: event.target.value});
        this.state.results.map(singleResult => {
            if(event.target.value === singleResult.name) {
                return this.pokemonActive(singleResult);
            } else {
                return null
            }
        });
    }

    pokemonActive(results) {
        fetch(results.url)
            .then(results => results.json())
            .then(data => {
                this.setState({
                    selectedResult: data
                })
            });
    }

    componentDidMount() {
        fetch('https://pokeapi.co/api/v2/pokemon')
            .then(results => results.json())
            .then(data => {
                this.setState({
                    results: data.results,
                    resultsLoad: data.results[0]
                });
            });
        // First pokemon on page load
        this.pokemonActive(this.state.resultsLoad)
    }

    render() {
        return (
            <div className="container pokedex-whole">
                <div className="row">
                    <Sidebar results={this.state.results} pokemonActive={this.pokemonActive.bind(this)}/>
                    <MainContent
                        profileResult={this.state.selectedResult}
                        seacrhPokemon={this.seacrhPokemon.bind(this)}
                    />

                </div>
            </div>
        )
    };
};

export default Pokedex;