Set Up Project
---
1) Clone repo
2) Run 'npm install'
3) Run 'npm start'

Build project for production
---
When the project is ready for production run the command 'yarn build'. For a full step to step guide of how to set up for Production you can visit https://medium.com/jeremy-gottfrieds-tech-blog/tutorial-how-to-deploy-a-production-react-app-to-heroku-c4831dfcfa08

